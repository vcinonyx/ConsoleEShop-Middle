﻿using System;
using ConsoleEshopLowLib.DB;
using ConsoleEshopLowLib.Models;
using ConsoleEshopLowLib.Users;
using ConsoleEshopLowLib.Enums;
using ConsoleEshopLowLib.Services;
using System.Collections.Generic;

namespace ConsoleAppEshop.ProcessHandlers
{
    public class GuestProcessHandler : IProcessHandler
    {
        private readonly UserService _userService;
        private readonly Dictionary<int, Action> ActionsMenu;
        public UserRole Role => UserRole.Guest;


        public GuestProcessHandler(UserService userService)
        {
            _userService = userService;
            ActionsMenu = new Dictionary<int, Action>()
            {
                { 3, () => Register() },
                { 4, () => Login()}
            };
        } 


        public void ProcessRequest(int i)
        {
            if (ActionsMenu.ContainsKey(i))
            {
                ActionsMenu[i].Invoke();
            }
            else
            {
                Console.WriteLine("Invalid Number");
            }        
        }

        public void Register()
        {
            Console.WriteLine("Enter your Name");
            var name = Console.ReadLine();
            Console.WriteLine("Enter your Surname");
            var surname = Console.ReadLine();
            Console.WriteLine("Enter new username:");
            var username = Console.ReadLine();
            Console.WriteLine("Enter new password:");
            var password = Console.ReadLine();
            Console.WriteLine(!_userService.Register(username, password)
                ? "User with such username already exist"
                : "Registration completed successfully");
        }

        public void Login()
        {
            Console.WriteLine("Enter username:");
            var username = Console.ReadLine();
            Console.WriteLine("Enter password:");
            var password = Console.ReadLine();
            Console.WriteLine(_userService.TryLogin(username, password)
                ? $"You are entered as {_userService.CurrentUser.Role}"
                : "No user with this username or password");
        }
    }
}

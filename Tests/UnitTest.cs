﻿using System;
using System.Collections.Generic;
using ConsoleEshopLowLib.Models;
using ConsoleEshopLowLib.Services;
using ConsoleEshopLowLib.Users;
using ConsoleEshopLowLib.Enums;
using ConsoleEshopLowLib.DB;
using NUnit.Framework;


namespace Tests
{
    public class Tests
    {
        [SetUp]

        public void SetUp()
        {
        }

        #region OrderServiceTests

        [Test]
        public void OrderServiceAddOrderReturnsTrue()
        {
            //Assign
            var orderService = new OrderService();

            //Act + Assert
        }
        [Test]
        public void OrderServiceContainOrderReturnsTrue()
        {
            //Assign
            var orderService = new OrderService();
            //Act
            var id = orderService.AddOrder(new List<(Product, int)>(), new AuthorizedUser());
            //Assert
            Assert.IsTrue(orderService.ContainsOrder(id));
        }
        [Test]
        public void OrderServiceGetOrderReturnsOrder()
        {
            //Assign
            var orderService = new OrderService();
            //Act
            var id = orderService.AddOrder(new List<(Product, int)>(), new AuthorizedUser());
            var order = orderService.GetOrder(id);
            //Assert
            Assert.IsTrue(order is Order);
        }

        [Test]
        public void OrderServiceOrderThrowsArgumentException()
        {
            //Assign
            var orderService = new OrderService();
            //Act
            try
            {
                var id = orderService.AddOrder(new List<(Product, int)>(), null);
            }
            // Assert
            catch (Exception ex)
            {
                Assert.IsTrue(ex is ArgumentNullException);
            }
        }
        #endregion

        #region ProductsServiceTests

        [Test]
        public void ProductsServiceAddProductReturnsTrue()
        {
            //Assign
            var productService = new ProductService();

            //Act + Assert
            Assert.IsTrue(productService.AddProduct("", "", "", 0));
        }
        [Test]
        public void ProductsServiceChangeProductInfoReturnsFalse()
        {
            //Assign
            var productService = new ProductService();
            //Act + Assert
            Assert.IsFalse(productService.ChangeProductInfo(1, new Product(), 10));
        }
        [Test]
        public void ProductsServiceContainsProductReturnsTrue()
        {
            //Assign
            var productService = new ProductService();
            productService.AddProduct("", "", "", 0);
            //Act + Assert
            Assert.IsTrue(productService.ContainsProduct(""));
        }
        #endregion

        #region UserServiceTests

        [Test]
        public void UserServiceRegisterReturnsTrue()
        {
            //Assign
            var userService = new UserService();

            //Act + Assert
            Assert.IsTrue(userService.Register("", ""));
        }
        [Test]
        public void UserServiceTryLoginReturnsFalse()
        {
            //Assign
            var userService = new UserService();

            //Act + Assert
            Assert.IsFalse(userService.TryLogin("", ""));
        }
        [Test]
        public void UserServiceEditUserInfoReturnsFalse()
        {
            //Assign
            var userService = new UserService();

            //Act + Assert
            Assert.IsFalse(userService.EditUserInfo(1, "", ""));
        }
        #endregion

    }
}

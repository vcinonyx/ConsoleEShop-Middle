﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleEshopLowLib
{
    public enum OrderStatus
    {
        New,
        CanceledByAdmin,
        PaymentReceived,
        Sent,
        Received,
        Completed,
        InProgress,
        CanceledByUser
    }
}

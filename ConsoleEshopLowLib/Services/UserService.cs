﻿using System;
using System.Collections.Generic;
using System.Linq;
using ConsoleEshopLowLib.DB;
using ConsoleEshopLowLib.Models;
using ConsoleEshopLowLib.Users;
using ConsoleEshopLowLib.Enums;

namespace ConsoleEshopLowLib.Services
{
    public class UserService
    {
        private readonly UsersDb _usersDb;
        public IUser CurrentUser { get; set; }

        public UserService()
        {
            _usersDb = new UsersDb();
        }

        public bool Register(string username, string password)
        {
            return _usersDb.AddUser(username, password);
        }

        public bool Register(string username, string password, string name, string surname)
        {
            var user = new AuthorizedUser(name, surname);
            return _usersDb.AddUser(username, password, user);
        }

        public bool TryLogin(string username, string password)
        {
            if (_usersDb.GetUser(username, password) is null)
            { 
                return false; 
            }

            CurrentUser = _usersDb.GetUser(username, password);
            return true;
        }

        public IUser GetUser(string username, string password)
        {
            return _usersDb.GetUser(username, password);
        }

        public void Logout()
        {
            CurrentUser = new Guest();
        }

        public List<AuthorizedUser> GetAuthorizedUsers()
        {
            return _usersDb.GetAuthorizedUsers();
        }

        public bool EditUserInfo(int id, string newName, string newSurname)
        {
            var authorizedUsers = GetAuthorizedUsers();
            var user = authorizedUsers.FirstOrDefault(x => x.Id == id);
            if (user is null)
            { 
                return false; 
            }
            else
            {
                user.Edit(newName, newSurname);
            }

            return true;
        }
    }
}

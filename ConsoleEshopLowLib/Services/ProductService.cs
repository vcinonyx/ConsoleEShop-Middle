﻿using System;
using System.Collections.Generic;
using System.Linq;
using ConsoleEshopLowLib.DB;
using ConsoleEshopLowLib.Models;

namespace ConsoleEshopLowLib.Services
{
    public class ProductService
    {
        private readonly ProductsDb _productsDb;

        public ProductService()
        {
            _productsDb = new ProductsDb();
        }

        public List<(Product, int)> GetProducts() 
        {
            return _productsDb.GetItems();
        }

        public (Product product, int quantity) GetProduct(string name)
        {
            return _productsDb.GetProduct(name);
        }

        public Product GetProduct(int id)
        {
            return _productsDb.GetProduct(id);
        }

        public bool AddProduct(string name, string description, string category, decimal price)
        {
            var product = new Product(name, description, category, price);

            return _productsDb.AddProduct(product);
        }

        public bool ContainsProduct(int id)
        {
            return _productsDb.ContainsProduct(id);
        }

        public bool ContainsProduct(string name)
        {
            return _productsDb.ContainsProduct(name);
        }

        public bool CheckInStock(IEnumerable<(Product product, int quantity)> products)
        {
            return _productsDb.CheckInStock(products);
        }

        public void ProductsReservation(IEnumerable<(Product, int)> products)
        {
            _productsDb.ProductsReservation(products);
        }

        public bool ChangeProductInfo(int id, Product newProduct, int newQuantity)
        {
            return _productsDb.ChangeProductInfo(id, newProduct, newQuantity);
        }

    }
}

﻿using ConsoleEshopLowLib.Models;
using System.Linq;
using System.Collections.Generic;
using System;

namespace ConsoleEshopLowLib.DB
{
    public class ProductsDb 
    {
        private readonly List<(Product product, int quantity)> _products = new List<(Product product, int quantity)>();
        public List<(Product product, int quantity)> GetItems() => _products;

        public Product GetProduct(int id)
        {
            return _products.Where(item => item.product.Id == id).Select(x => x.product).FirstOrDefault();
        }

        public (Product product, int quantity) GetProduct(string name)
        {
            if (string.IsNullOrEmpty(name))
                throw new ArgumentException("Product's name cannot be empty or null", nameof(name));

            return _products.FirstOrDefault(item => item.product.Name.ToUpper() == name.ToUpper());
        }        

        public bool AddProduct(Product product, int quantity = 1)
        {
            if (product == null)
            {
                throw new ArgumentNullException(nameof(product));
            }
            if (_products.Exists(x => x.product.Id == product.Id))
            {
                return false;
            }

            _products.Add((product, quantity));
            return true;
        }

        public bool ContainsProduct(string name)
        {
            return _products.Any(x => x.product.Name == name);
        }

        public bool ContainsProduct(int id)
        {
            return _products.Any(x => x.product.Id == id);
        }

        public bool CheckInStock(IEnumerable<(Product product, int quantity)> products)
        {
            foreach (var (product, quantity) in products)
            {
                if (_products.Find(x => Equals(x.product, product)).quantity < quantity)
                {
                    return false;
                }
            }

            return true;
        }

        public bool ChangeProductInfo(int id, Product newProduct, int quantity)
        {
            if (!_products.Exists(x => x.product.Id == id)) 
            { 
                return false; 
            }
            
            var existProduct = _products.Find(x => x.product.Id == id);
            _products.Remove(existProduct);
            existProduct.product.InfoFromProduct(newProduct);
            existProduct.quantity = quantity;
            _products.Add(existProduct);
            return true;
        }

        public void ProductsReservation(IEnumerable<(Product, int)> productsList)
        {
            foreach (var (product, quantity) in productsList)
            {
                var existProduct = _products.Find(x => x.product.Id == product.Id);
                _products.Remove(existProduct);
                existProduct.quantity -= quantity;
                _products.Add(existProduct);
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;
using ConsoleEshopLowLib.Enums;

namespace ConsoleAppEshop.ProcessHandlers
{
    public interface IProcessHandler
    {
        public UserRole Role { get; }
        void ProcessRequest(int i);
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleEshopLowLib.Models
{
    public class Product
    {
        private static int _counterId = 1;
        private decimal _price;
        public int Id { get; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Category { get; set; }
        public decimal Price 
        {
            get => _price;

            set
            {
                if (value < 0) 
                    throw new ArgumentException("Price cannot be negative");

                _price = value;
            }
        }


        public Product()
        {
            Id = _counterId++;
        }

        public Product(Product newProduct)
        {
            Name = newProduct.Name;
            Category = newProduct.Category;
            Description = newProduct.Description;
            Price = newProduct.Price;
        }

        public Product(string name, string description, string category, decimal price)
        {
            Name = name;
            Description = description;
            Category = category;
            Price = price;
            Id = _counterId++;
        }

        public void InfoFromProduct(Product newProduct)
        {
            Name = newProduct.Name;
            Category = newProduct.Category;
            Description = newProduct.Description;
            Price = newProduct.Price;
        }
    }
}

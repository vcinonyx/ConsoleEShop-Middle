﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using ConsoleEshopLowLib.Users;


namespace ConsoleEshopLowLib.Models
{
    public class Order : IEnumerable<(Product, int)>
    {
        private static int _counterId;
        public OrderStatus Status = OrderStatus.New;
        private List<(Product Product, int Quantity)> _products;
        public IEnumerable<(Product, int)> GetProducts => _products;
        public int Id { get; }
        public int CustomerId { get; }
        public decimal TotalSum => _products.Sum(product => product.Product.Price * product.Quantity);

        public Order(IEnumerable<(Product product, int quantity)> products, IUser user)
        {
            if (products == null)
                throw new ArgumentNullException(nameof(products));

            if (user == null)
                throw new ArgumentNullException(nameof(user));

            _products = new List<(Product Product, int Quantity)>();

            foreach (var (prod, quant) in products)
            {
                AddItem(prod, quant);
            }

            
            CustomerId = user.Id;
            Id = _counterId++;
        }

        public void AddItem(Product product, int quantity = 1)
        {
            if (product == null) 
                throw new ArgumentNullException(nameof(product));

            if (quantity <= 0)
                throw new ArgumentException("Qunatity should be greater than 0", nameof(quantity));

            _products.Add((product, quantity));
        }

        public IEnumerator<(Product, int)> GetEnumerator()
        {
            return _products.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return this.GetEnumerator();
        }

    }
}

﻿using System;
using ConsoleEshopLowLib;
using ConsoleEshopLowLib.Models;
using ConsoleEshopLowLib.DB;
using ConsoleAppEshop.ProcessHandlers;
using ConsoleEshopLowLib.Enums;
using System.Collections.Generic;

namespace ConsoleAppEshop
{
    class Program
    {
        static void Main(string[] args)
        {
            Dictionary<UserRole, Action> ActionMenu = new Dictionary<UserRole, Action>();

            ActionMenu.Add(UserRole.Guest, () =>
            {
                Console.WriteLine("3 - user account registration;");
                Console.WriteLine("4 - login to the online store with an account.");
            });

            ActionMenu.Add(UserRole.AuthorizedUser, () =>
            {
                Console.WriteLine("3 - creating a new order;");
                Console.WriteLine("4 - ordering or cancellation;");
                Console.WriteLine("5 - view order history and delivery status;");
                Console.WriteLine("6 - setting the status of the order \"Received\";");
                Console.WriteLine("7 - change of personal information;");
                Console.WriteLine("8 - sign out of your account.");
            });

            ActionMenu.Add(UserRole.Admin, () =>
            {
                Console.WriteLine("3 - creating a new order");
                Console.WriteLine("4 - ordering;");
                Console.WriteLine("5 - view and change users personal information;");
                Console.WriteLine("6 - adding a new product (name, category, description, cost);");
                Console.WriteLine("7 - change of product information;");
                Console.WriteLine("8 - change the status of the order;");
                Console.WriteLine("9 - edit users info");
                Console.WriteLine("10 - sign out of your account.");

            });

            var eshop = new EshopRequestHandler();

            Console.WriteLine("Welcome to the our Eshop!");
            while (true)
            {
                Console.WriteLine("1 - view the list of goods;");
                Console.WriteLine("2 - search for a product by name;");
                ActionMenu[eshop.GetCurrentUserRole()].Invoke();

                int.TryParse(Console.ReadLine(), out var i);
                try
                {
                    eshop.DelegateRequest(i);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }
            }
        }
    }
}
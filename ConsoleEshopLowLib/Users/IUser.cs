﻿using System;
using System.Collections.Generic;
using System.Text;
using ConsoleEshopLowLib.Enums;


namespace ConsoleEshopLowLib.Users
{
    public interface IUser
    {
        public int Id { get; }
        public UserRole Role { get; }
    }
}
